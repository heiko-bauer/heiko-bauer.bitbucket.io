'use strict';

let game = new function() {
	this.player = 1;
	this.values = [];
	
	for ( let i = 0; i < 9; ++i )
	{
		let btn = document.getElementById( "btn" + i.toString() );
		this.values.push( 0 ); // 0 means not assigned
		btn.onclick = () => { this.onClick( i ) };
	}
	
	this.onClick = function( index ) 
	{
		if ( this.values[ index ] != 0 || this.playerWins() )
			return;
		
		this.values[ index ] = this.player;
		let btn = document.getElementById( "btn" + index.toString() );
		btn.innerHTML = this.player == 1 ? "X" : "O";
		
		let res = document.getElementById( "res" );
		let bWinner = this.playerWins();
		let bDraw = (! bWinner) && (! this.hasOpenFields());
		if ( bDraw )
		{
			res.innerHTML = "Draw Game. Press F5 to play again";
		}
		else if ( bWinner )
		{
			res.innerHTML = this.player == 1 ? "Player one wins. Press F5 to play again" : "Player two wins. Press F5 to play again";
		}
		else
		{
			res.innerHTML = this.player == 1 ? "player two's turn" : "player one's turn";
			this.player = this.player == 1 ? 2 : 1;
		}
	}
	
	this.playerWins = function()
	{
		// check rows
		if ( this.values[ 0 ] == this.player && this.values[ 1 ] == this.player && this.values[ 2 ] == this.player )
			return true;
		if ( this.values[ 3 ] == this.player && this.values[ 4 ] == this.player && this.values[ 5 ] == this.player )
			return true;
		if ( this.values[ 6 ] == this.player && this.values[ 7 ] == this.player && this.values[ 8 ] == this.player )
			return true;
		
		// check columns
		if ( this.values[ 0 ] == this.player && this.values[ 3 ] == this.player && this.values[ 6 ] == this.player )
			return true;
		if ( this.values[ 1 ] == this.player && this.values[ 4 ] == this.player && this.values[ 7 ] == this.player )
			return true;
		if ( this.values[ 2 ] == this.player && this.values[ 5 ] == this.player && this.values[ 8 ] == this.player )
			return true;

		// check diagonal
		if ( this.values[ 0 ] == this.player && this.values[ 4 ] == this.player && this.values[ 8 ] == this.player )
			return true;
		if ( this.values[ 2 ] == this.player && this.values[ 4 ] == this.player && this.values[ 6 ] == this.player )
			return true;
		
		return false;
	}
	
	this.hasOpenFields = function()
	{
		for ( let v of this.values )
		{
			if ( v == 0 )
				return true;
		}
		
		return false;
	}
};
